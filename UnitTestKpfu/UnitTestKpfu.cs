﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    public class TestKpfu
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver(@"C:\Server"); ;
            baseURL = "http://kpfu.ru/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheKpfuTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.LinkText("Вход")).Click();
            driver.FindElement(By.Name("p_login")).Clear();
            driver.FindElement(By.Name("p_login")).SendKeys("GDSharipova");
            driver.FindElement(By.Name("p_pass")).Clear();
            driver.FindElement(By.Name("p_pass")).SendKeys("mohnttrx");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
            driver.FindElement(By.XPath("//div[@id='clean_area']/div[2]/div/table/tbody/tr[2]/td[2]/a/div[2]")).Click();
            driver.FindElement(By.LinkText("Обо мне")).Click();
            driver.FindElement(By.Name("p_phone")).Clear();
            driver.FindElement(By.Name("p_phone")).SendKeys("+79047633595");
            driver.FindElement(By.CssSelector("input[type=\"button\"]")).Click();
            driver.FindElement(By.LinkText("Выход")).Click();
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}